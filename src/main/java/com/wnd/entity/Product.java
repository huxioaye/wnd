package com.wnd.entity;

import java.util.List;

public class Product {
    private Integer pid;

    private Shop shop;

    private String proName;

    private Double price;

    private Integer proTotal;
    private String  sort;
    private ProductNav productNav;
    private String   img;

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public Integer getPid() {
        return pid;
    }

    public void setPid(Integer pid) {
        this.pid = pid;
    }



    public String getProName() {
        return proName;
    }

    public void setProName(String proName) {
        this.proName = proName == null ? null : proName.trim();
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Integer getProTotal() {
        return proTotal;
    }

    public void setProTotal(Integer proTotal) {
        this.proTotal = proTotal;
    }

    public Shop getShop() {
        return shop;
    }

    public void setShop(Shop shop) {
        this.shop = shop;
    }

    public ProductNav getProductNav() {
        return productNav;
    }

    public void setProductNav(ProductNav productNav) {
        this.productNav = productNav;
    }
}