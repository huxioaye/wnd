package com.wnd.entity.vo;

import com.wnd.entity.CommentTwo;

public class CommentPermissionVo {
    private String username;
    private String userImg;
    private String context;
    private String commentTime;
    private String commentState;
    private CommentTwo commentTwo;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUserImg() {
        return userImg;
    }

    public void setUserImg(String userImg) {
        this.userImg = userImg;
    }

    public String getContext() {
        return context;
    }

    public void setContext(String context) {
        this.context = context;
    }

    public String getCommentTime() {
        return commentTime;
    }

    public void setCommentTime(String commentTime) {
        this.commentTime = commentTime;
    }

    public String getCommentState() {
        return commentState;
    }

    public void setCommentState(String commentState) {
        this.commentState = commentState;
    }

    public CommentTwo getCommentTwo() {
        return commentTwo;
    }

    public void setCommentTwo(CommentTwo commentTwo) {
        this.commentTwo = commentTwo;
    }

    @Override
    public String toString() {
        return "CommentPermissionVo{" +
                "username='" + username + '\'' +
                ", userImg='" + userImg + '\'' +
                ", context='" + context + '\'' +
                ", commentTime='" + commentTime + '\'' +
                ", commentState='" + commentState + '\'' +
                ", commentTwo=" + commentTwo +
                '}';
    }
}
