package com.wnd.entity.vo;

import com.wnd.entity.Product;
import com.wnd.entity.ProductNav;
import com.wnd.entity.Shop;

import java.util.List;
import java.util.Map;

public class ProductPermissionVo {
    private ShopPermissionVo shop;
    private List<ProductNav> productNavs;
    private List<Product> products;
    private Map<String ,List<Product>> productsAndNav;
    
    public ShopPermissionVo getShop() {
        return shop;
    }

    public void setShop(ShopPermissionVo shop) {
        this.shop = shop;
    }

    public List<ProductNav> getProductNavs() {
        return productNavs;
    }

    public void setProductNavs(List<ProductNav> productNavs) {
        this.productNavs = productNavs;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }

    public Map<String, List<Product>> getProductsAndNav() {
        return productsAndNav;
    }

    public void setProductsAndNav(Map<String, List<Product>> productsAndNav) {
        this.productsAndNav = productsAndNav;
    }
}
