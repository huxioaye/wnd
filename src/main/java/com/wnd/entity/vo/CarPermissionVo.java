package com.wnd.entity.vo;

import com.wnd.entity.Product;
import com.wnd.entity.ProductNav;
import com.wnd.entity.Shop;

import java.util.List;

public class CarPermissionVo {
    private int cid;
    private String time;
    private int count;
    private int pid;
    private String proName;
    private String img;
    private Double price;
    private Shop shop;
    private Integer proTotal;
    private String  sort;

    public int getCid() {
        return cid;
    }

    public void setCid(int cid) {
        this.cid = cid;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getPid() {
        return pid;
    }

    public void setPid(int pid) {
        this.pid = pid;
    }

    public String getProName() {
        return proName;
    }

    public void setProName(String proName) {
        this.proName = proName;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Shop getShop() {
        return shop;
    }

    public void setShop(Shop shop) {
        this.shop = shop;
    }

    public Integer getProTotal() {
        return proTotal;
    }

    public void setProTotal(Integer proTotal) {
        this.proTotal = proTotal;
    }

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }
}
