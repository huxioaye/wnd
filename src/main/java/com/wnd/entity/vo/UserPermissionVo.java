package com.wnd.entity.vo;

import com.wnd.entity.Authority;
import com.wnd.enums.UserEnums;
import com.wnd.enums.UserStateEnums;

import java.util.List;

/**
 * 查询数据的综合体,不是单表
 */
public class UserPermissionVo {
    private Integer uid;

    private String username;

    private String password;

    private String sex;

    private String img;

    private String phone;

    private Double balance;

    private UserEnums vipstate;

    private UserStateEnums state;
    private String time;
    private Authority authority;
    private String address;
    private int dateCount;

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getDateCount() {
        return dateCount;
    }

    public void setDateCount(int dateCount) {
        this.dateCount = dateCount;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getUid() {
        return uid;
    }

    public void setUid(Integer uid) {
        this.uid = uid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username == null ? null : username.trim();
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password == null ? null : password.trim();
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex == null ? null : sex.trim();
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img == null ? null : img.trim();
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone == null ? null : phone.trim();
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public UserEnums getVipstate() {
        return vipstate;
    }

    public void setVipstate(UserEnums vipstate) {
        this.vipstate = vipstate;
    }

    public UserStateEnums getState() {
        return state;
    }

    public void setState(UserStateEnums state) {
        this.state = state;
    }

    public Authority getAuthority() {
        return authority;
    }

    public void setAuthority(Authority authority) {
        this.authority = authority;
    }

    @Override
    public String toString() {
        return "UserPermissionVo{" +
                "uid=" + uid +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", sex='" + sex + '\'' +
                ", img='" + img + '\'' +
                ", phone='" + phone + '\'' +
                ", balance=" + balance +
                ", vipstate=" + vipstate +
                ", state=" + state +
                ", time='" + time + '\'' +
                ", authority=" + authority +
                ", address='" + address + '\'' +
                ", dateCount=" + dateCount +
                '}';
    }
}
