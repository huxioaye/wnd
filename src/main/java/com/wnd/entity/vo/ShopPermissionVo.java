package com.wnd.entity.vo;

public class ShopPermissionVo {
    private Integer sid;
    private String  shopName;//店铺名称
    private String  shopInfo;//店铺信息
    private String  img;//店铺图片
    private String  shopAddressProvince;//省
    private String  shopAddressCity;//市
    private String  shopAddressArea;//区
    private String  shopAddressDetail;//详细地址
    private String  prototal;//销售总量
    private double  avergePrice;//平均消费
    private String  time;//配送时间
    private double  timePrice;//配送费
    private String  sort;

    public String getSort() {
        return sort;
    }

    public void setSort(String sort) {
        this.sort = sort;
    }

    public double getTimePrice() {
        return timePrice;
    }

    public void setTimePrice(double timePrice) {
        this.timePrice = timePrice;
    }

    public ShopPermissionVo(Integer sid, String shopName, String shopInfo, String img, String shopAddressProvince, String shopAddressCity, String shopAddressArea, String shopAddressDetail, String prototal, double avergePrice, String time) {
        this.sid = sid;
        this.shopName = shopName;
        this.shopInfo = shopInfo;
        this.img = img;
        this.shopAddressProvince = shopAddressProvince;
        this.shopAddressCity = shopAddressCity;
        this.shopAddressArea = shopAddressArea;
        this.shopAddressDetail = shopAddressDetail;
        this.prototal = prototal;
        this.avergePrice = avergePrice;
        this.time = time;
    }

    public ShopPermissionVo() {
    }

    public Integer getSid() {
        return sid;
    }

    public void setSid(Integer sid) {
        this.sid = sid;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getShopInfo() {
        return shopInfo;
    }

    public void setShopInfo(String shopInfo) {
        this.shopInfo = shopInfo;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getShopAddressProvince() {
        return shopAddressProvince;
    }

    public void setShopAddressProvince(String shopAddressProvince) {
        this.shopAddressProvince = shopAddressProvince;
    }

    public String getShopAddressCity() {
        return shopAddressCity;
    }

    public void setShopAddressCity(String shopAddressCity) {
        this.shopAddressCity = shopAddressCity;
    }

    public String getShopAddressArea() {
        return shopAddressArea;
    }

    public void setShopAddressArea(String shopAddressArea) {
        this.shopAddressArea = shopAddressArea;
    }

    public String getShopAddressDetail() {
        return shopAddressDetail;
    }

    public void setShopAddressDetail(String shopAddressDetail) {
        this.shopAddressDetail = shopAddressDetail;
    }

    public String getPrototal() {
        return prototal;
    }

    public void setPrototal(String prototal) {
        this.prototal = prototal;
    }

    public double getAvergePrice() {
        return avergePrice;
    }

    public void setAvergePrice(double avergePrice) {
        this.avergePrice = avergePrice;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
