package com.wnd.entity;

public class Authority {
    private Integer aid;

    private String authorityName;

    private String authorityGreat;

    public Integer getAid() {
        return aid;
    }

    public void setAid(Integer aid) {
        this.aid = aid;
    }

    public String getAuthorityName() {
        return authorityName;
    }

    public void setAuthorityName(String authorityName) {
        this.authorityName = authorityName == null ? null : authorityName.trim();
    }

    public String getAuthorityGreat() {
        return authorityGreat;
    }

    public void setAuthorityGreat(String authorityGreat) {
        this.authorityGreat = authorityGreat;
    }
}