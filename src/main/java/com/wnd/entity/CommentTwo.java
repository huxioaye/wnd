package com.wnd.entity;

import java.util.Date;

public class CommentTwo {
    private Integer twid;

    private CommentFirst commentFirst;

    private String context;

    private String time;

    private Merchant merchant;

    public Integer getTwid() {
        return twid;
    }

    public void setTwid(Integer twid) {
        this.twid = twid;
    }

    public String getContext() {
        return context;
    }

    public void setContext(String context) {
        this.context = context == null ? null : context.trim();
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public CommentFirst getCommentFirst() {
        return commentFirst;
    }

    public void setCommentFirst(CommentFirst commentFirst) {
        this.commentFirst = commentFirst;
    }

    public Merchant getMerchant() {
        return merchant;
    }

    public void setMerchant(Merchant merchant) {
        this.merchant = merchant;
    }

    @Override
    public String toString() {
        return "CommentTwo{" +
                "twid=" + twid +
                ", commentFirst=" + commentFirst +
                ", context='" + context + '\'' +
                ", time=" + time +
                ", merchant=" + merchant +
                '}';
    }
}