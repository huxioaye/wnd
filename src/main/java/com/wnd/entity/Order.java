package com.wnd.entity;

import java.util.Date;

public class Order {
    private Integer oid;

    private Date oTime;

    private Product product;

    private Integer proCount;

    private User user;

    private Shop shop;

    private Double oTotalMoney;

    private String oState;

    public Integer getOid() {
        return oid;
    }

    public void setOid(Integer oid) {
        this.oid = oid;
    }

    public Date getoTime() {
        return oTime;
    }

    public void setoTime(Date oTime) {
        this.oTime = oTime;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Integer getProCount() {
        return proCount;
    }

    public void setProCount(Integer proCount) {
        this.proCount = proCount;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Shop getShop() {
        return shop;
    }

    public void setShop(Shop shop) {
        this.shop = shop;
    }

    public Double getoTotalMoney() {
        return oTotalMoney;
    }

    public void setoTotalMoney(Double oTotalMoney) {
        this.oTotalMoney = oTotalMoney;
    }

    public String getoState() {
        return oState;
    }

    public void setoState(String oState) {
        this.oState = oState;
    }
}