package com.wnd.entity;

import java.util.Date;

public class Blog {
    private Integer bid;

    private String oppration;

    private Date blogTime;

    public Integer getBid() {
        return bid;
    }

    public void setBid(Integer bid) {
        this.bid = bid;
    }

    public String getOppration() {
        return oppration;
    }

    public void setOppration(String oppration) {
        this.oppration = oppration == null ? null : oppration.trim();
    }

    public Date getBlogTime() {
        return blogTime;
    }

    public void setBlogTime(Date blogTime) {
        this.blogTime = blogTime;
    }
}