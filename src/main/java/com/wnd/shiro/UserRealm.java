package com.wnd.shiro;

import com.wnd.entity.User;
import com.wnd.entity.vo.UserPermissionVo;
import com.wnd.service.UserService;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 权限类
 */
@Component
public class UserRealm extends AuthorizingRealm {
    @Autowired
    private UserService  userService;

    /**
     * 权限管理
     * @param principalCollection
     * @return
     */
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        return null;
    }

    /**
     * 获取用户认证信息
     * @param token
     * @return
     * @throws AuthenticationException
     */
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken token) throws AuthenticationException {
        String username= (String) token.getPrincipal();
        UserPermissionVo user= userService.findByUsername(username);

        if (user==null){
            //用户名不存在，直接抛出异常
            throw  new UnknownAccountException("用户名不存在");
        }
        SimpleAuthenticationInfo authenticationInfo=new SimpleAuthenticationInfo(
                user.getUsername(),
                user.getPassword(),
                getName()
        );
        return authenticationInfo;
    }
}
