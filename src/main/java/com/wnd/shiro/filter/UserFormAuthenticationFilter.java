package com.wnd.shiro.filter;

import com.alibaba.fastjson.JSONObject;
import com.wnd.common.JsonResult;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.authc.FormAuthenticationFilter;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;

/**
 * 自定义表单认证过滤器
 */
public class UserFormAuthenticationFilter extends FormAuthenticationFilter {
    //访问需要登录才能访问的url时，当未登录或登陆过期，访问被拒绝后，执行
    @Override
    protected boolean onAccessDenied(ServletRequest request, ServletResponse response) throws Exception {
        //获取httpServletResponse对象
        HttpServletResponse  servletResponse= (HttpServletResponse) response;
        //设置响应头类型和编码
        servletResponse.setHeader("Content-Type","application/json;charset=utf-8");
        //获取PrintWriter对象
        PrintWriter writer = servletResponse.getWriter();
        //创建JsonResult对象
        JsonResult jsonResult = new JsonResult(-1, "未登录或登录过期", null);
        //JsonResult转换成json字符串
        String  json= JSONObject.toJSONString(jsonResult);
        //PrintWriter对象把json字符串写入输出流中
        writer.write(json);
        //刷新
        writer.flush();
        //关闭流
        writer.close();
        //return false  不再跳转页面
        return false;
    }

    @Override
    protected boolean isAccessAllowed(ServletRequest request, ServletResponse response, Object mappedValue) {
        //获取Subject主体
        Subject subject=getSubject(request,response);
        //已经认证过的访问或者rememberMe的访问也放行
        return subject.isAuthenticated()||subject.isRemembered();
    }
}
