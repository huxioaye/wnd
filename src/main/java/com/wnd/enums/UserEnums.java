package com.wnd.enums;

/**
 * 用户会员信息和判断是否发言
 */
public enum UserEnums {
    NO(0,"否"),
    YES(1,"是");
    private int  code;
    private String name;

    public static UserEnums getByCode(int code){
        //获取SexEnums所有枚举值
        UserEnums[] values=UserEnums.values();
        //遍历SexEnums所有的枚举值，
        //判断枚举值的code编码是否等于传进来参数的code值
        //如果相等，就直接返回value值，也就枚举值
        //都不等于则返回null
        for (UserEnums value:values){
            if (value.getCode()==code){
                return value;
            }
        }
        return null;
    }

     UserEnums() {
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    UserEnums(int code, String name) {
        this.code = code;
        this.name = name;
    }
}
