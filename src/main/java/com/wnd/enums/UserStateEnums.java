package com.wnd.enums;

/**
 * 判断用户启用封禁状态
 */
public enum UserStateEnums {
    NO(0,"封禁"),
   YES(1,"启用");
    private int  code;
    private String name;

    public static UserStateEnums getByCode(int code){
        //获取SexEnums所有枚举值
        UserStateEnums[] values=UserStateEnums.values();
        //遍历SexEnums所有的枚举值，
        //判断枚举值的code编码是否等于传进来参数的code值
        //如果相等，就直接返回value值，也就枚举值
        //都不等于则返回null
        for (UserStateEnums value:values){
            if (value.getCode()==code){
                return value;
            }
        }
        return null;
    }

    UserStateEnums() {
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    UserStateEnums(int code, String name) {
        this.code = code;
        this.name = name;
    }
}
