package com.wnd.enums;

/**
 * 可用于判断骑手的审核和商家的审核
 */
public enum AuditEnums {
    NO(0,"未通过"),
    YES(1,"已通过"),
    FORBID(2,"禁用");
    private int  code;
    private String name;
    public static AuditEnums getByCode(int code){
        //获取SexEnums所有枚举值
        AuditEnums[] values= AuditEnums.values();
        //遍历SexEnums所有的枚举值，
        //判断枚举值的code编码是否等于传进来参数的code值
        //如果相等，就直接返回value值，也就枚举值
        //都不等于则返回null
        for (AuditEnums value:values){
            if (value.getCode()==code){
                return value;
            }
        }
        return null;
    }

    AuditEnums() {
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    AuditEnums(int code, String name) {
        this.code = code;
        this.name = name;
    }
}
