package com.wnd.enums;

/**
 * 订单状态
 */
public enum PlatFormEnums {
    NO(0,"未接单"),
    NOW(1,"进行中"),
    YES(2,"已接单");
    private int  code;
    private String name;
    public static PlatFormEnums getByCode(int code){
        //获取SexEnums所有枚举值
        PlatFormEnums[] values= PlatFormEnums.values();
        //遍历SexEnums所有的枚举值，
        //判断枚举值的code编码是否等于传进来参数的code值
        //如果相等，就直接返回value值，也就枚举值
        //都不等于则返回null
        for (PlatFormEnums value:values){
            if (value.getCode()==code){
                return value;
            }
        }
        return null;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    PlatFormEnums() {
    }

    PlatFormEnums(int code, String name) {
        this.code = code;
        this.name = name;
    }
}
