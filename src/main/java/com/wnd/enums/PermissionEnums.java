package com.wnd.enums;

/**
 * 权限管理
 */
public enum PermissionEnums {
    ROOT(0,"超级管理员"),
    MANAGER(1,"管理员"),
    USER(2,"用户"),
    SHOPS(3,"商家"),
    RIDER(4,"骑手");
    private int  code;
    private String name;
    public static PermissionEnums getByCode(int code){
        //获取SexEnums所有枚举值
        PermissionEnums[] values= PermissionEnums.values();
        //遍历SexEnums所有的枚举值，
        //判断枚举值的code编码是否等于传进来参数的code值
        //如果相等，就直接返回value值，也就枚举值
        //都不等于则返回null
        for (PermissionEnums value:values){
            if (value.getCode()==code){
                return value;
            }
        }
        return null;
    }
    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    PermissionEnums(int code, String name) {
        this.code = code;
        this.name = name;
    }

    PermissionEnums() {
    }
}
