package com.wnd.enums;

public enum OrderEnums {
    NO(0,"未支付"),
    NOW(1,"已支付"),
    YES(2,"已退款");
    private int  code;
    private String name;
    public static OrderEnums getByCode(int code){
        //获取SexEnums所有枚举值
        OrderEnums[] values= OrderEnums.values();
        //遍历SexEnums所有的枚举值，
        //判断枚举值的code编码是否等于传进来参数的code值
        //如果相等，就直接返回value值，也就枚举值
        //都不等于则返回null
        for (OrderEnums value:values){
            if (value.getCode()==code){
                return value;
            }
        }
        return null;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    OrderEnums() {
    }

    OrderEnums(int code, String name) {
        this.code = code;
        this.name = name;
    }
}
