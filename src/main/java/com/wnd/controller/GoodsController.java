package com.wnd.controller;

import com.wnd.common.JsonResult;
import com.wnd.service.GoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@RequestMapping("goods")
@Controller
public class GoodsController {
    @Autowired
    private GoodsService goodsService;
    @CrossOrigin
    @ResponseBody
    @RequestMapping("goodsSelectAll")
    public JsonResult  goodsSelectAll(@RequestParam("id")String id){
        return new JsonResult(0,"请求成功",goodsService.SelectAll(Integer.parseInt(id)));
    }

}
