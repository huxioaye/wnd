package com.wnd.controller;

import com.wnd.common.JsonResult;
import com.wnd.entity.Order;
import com.wnd.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@RequestMapping("shopCart")
@Controller
public class ShopCartController {
    @Autowired
    private OrderService orderService;
    @CrossOrigin
    @RequestMapping("selectOrder")
    @ResponseBody
    public JsonResult  selectOrder(@RequestParam("userid")int userid,
                              @RequestParam("shopid")int shopid){
        return new JsonResult(0,"修改成功",orderService.selectOrderAll(userid,shopid));
    }
    @CrossOrigin
    @RequestMapping("update")
    @ResponseBody
    public JsonResult  update(@RequestParam("userid")int userid,
                              @RequestParam("productid")int productid,
                              @RequestParam("shopid")int shopid,
                              @RequestParam("count") int count){
        System.out.println(userid+"------"+productid+"------"+shopid+"------"+count);
        return new JsonResult(0,"修改成功",orderService.updateOrder(userid,productid,shopid,count));
    }

    @CrossOrigin
    @RequestMapping("deleteAllOrder")
    @ResponseBody
    public JsonResult  deleteAllOrder(@RequestParam("userid")int userid,
                              @RequestParam("shopid")int shopid){
        return new JsonResult(0,"清空成功",orderService.deleteAllOrder(userid,shopid));
    }
}
