package com.wnd.controller;

import com.hazelcast.internal.json.Json;
import com.wnd.common.JsonResult;
import com.wnd.service.ShopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("shop")
public class ShopController {
   @Autowired
   private ShopService shopService;

   /**
    * 查找商铺类型
    * @return
    */
   @CrossOrigin
   @RequestMapping("sort")
   @ResponseBody
   public JsonResult sort(){
       return new JsonResult(0,"请求成功",shopService.selectAll());
   }

   /**
    * 查找最近商铺
    * @return
    */
   @CrossOrigin
   @ResponseBody
   @RequestMapping("nearbyshopList")
   public JsonResult nearbyshopList(@RequestParam("address")String address,@RequestParam("cate") String cate){
      return new JsonResult(0,"请求成功",shopService.selectAllShop(address,cate));
   }
   @CrossOrigin
   @ResponseBody
   @RequestMapping("SelectShopByName")
   public JsonResult SelectShopByName(@RequestParam("shopName")String shopName){
      return new JsonResult(0,"请求成功",shopService.SelectShopByName(shopName));
   }
}
