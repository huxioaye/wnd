package com.wnd.controller;

import com.wnd.common.JsonResult;
import com.wnd.service.CommentFirstService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@RequestMapping("comment")
@Controller
public class CommentController {
    @Autowired
    private CommentFirstService commentFirstService;
    @RequestMapping("selectBySid")
    @CrossOrigin
    @ResponseBody
    public JsonResult selectBySid(@RequestParam("sid") Integer sid){
        return new JsonResult(0,"请求成功",commentFirstService.selectCommentBySid(sid));
    }
}
