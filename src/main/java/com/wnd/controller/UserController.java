package com.wnd.controller;

import com.wnd.common.JsonResult;
import com.wnd.entity.User;
import com.wnd.entity.vo.UserPermissionVo;
import com.wnd.enums.UserEnums;
import com.wnd.enums.UserStateEnums;
import com.wnd.service.UserService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

@Controller
@RequestMapping("user")
@CrossOrigin
public class UserController {
    @Autowired
    private UserService  userService;
    @ResponseBody
    @RequestMapping("login")
    @CrossOrigin
    public JsonResult   login(@RequestParam("username")String username,
                              @RequestParam("password")String password,
                              @RequestParam(value = "rememberMe",required = false,defaultValue = "false")
                              Boolean  rememberMe){
        UsernamePasswordToken  token=new UsernamePasswordToken(username,password);
        token.setRememberMe(rememberMe);
        SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            SecurityUtils.getSubject().login(token);
            UserPermissionVo user= userService.findByUsername(username);
            Date d1=sdf.parse(user.getTime());
            Date d2=sdf.parse(sdf.format(new Date()));
           if (user.getVipstate().equals(UserEnums.YES)){
               int daysBetween= (int) ((d2.getTime()-d1.getTime())/(60*60*24*1000));
               user.setDateCount(30-daysBetween);
           }else {
               user.setDateCount(0);
           }
            System.out.println(user);
            if (user.getState().equals("封禁")){
                throw new RuntimeException();
            }
            return new JsonResult(0,"登陆成功",user);
        }catch (UnknownAccountException e){
            return new JsonResult(1,"用户名错误",null);
        }catch (IncorrectCredentialsException e) {
            return new JsonResult(1, "密码错误", null);
        } catch (RuntimeException e) {
            e.printStackTrace();
            return new JsonResult(1,"用户已被封禁",null);
        } catch (ParseException e) {
            e.printStackTrace();
            return new JsonResult(1,"登录信息错误",null);
        }
    }
    @RequestMapping("logout")
    @ResponseBody
    @CrossOrigin
    public JsonResult  logout(){
        SecurityUtils.getSubject().logout();
        return new JsonResult(0,"登出成功",null);
    }
    @RequestMapping(value = "selectByName")
    @CrossOrigin
    @ResponseBody
    public JsonResult selectByName(@RequestParam("username") String username){
        return new JsonResult(0,userService.selectByName(username),null);
    }
    @ResponseBody
    @RequestMapping("register")
    @CrossOrigin
    public JsonResult  register(String username,String password,String phone){
        return new JsonResult(0,userService.addUser(username,password,phone),null);
    }
}
