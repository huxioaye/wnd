package com.wnd.dao;

import com.wnd.entity.ShopSort;

import java.util.List;

public interface ShopSortMapper {

    List<ShopSort> selectAll();
}