package com.wnd.dao;

import com.wnd.entity.vo.CommentPermissionVo;

import java.util.List;

public interface CommentFirstMapper {
    List<CommentPermissionVo>   selectCommentBySid(Integer sid);
}