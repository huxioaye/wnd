package com.wnd.dao;

import com.wnd.entity.Shop;
import com.wnd.entity.vo.ShopPermissionVo;

import java.util.List;
import java.util.Map;

public interface ShopMapper {

    List<ShopPermissionVo> selectAllShop(Map map);

    List<ShopPermissionVo> SelectShopByName(String shopName);
}