package com.wnd.dao;

import com.wnd.entity.Car;
import com.wnd.entity.Product;
import com.wnd.entity.vo.CarPermissionVo;

import java.util.List;
import java.util.Map;

public interface OrderMapper {

    Car selectOrder(Map map);

    int addOrder(Map map);

    int deleteOrder(Map map);

    int updateOrder(Map map);

    List<CarPermissionVo> selectOrderAll(Map map);

    int deleteAllOrder(Map map);
}