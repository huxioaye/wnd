package com.wnd.dao;

import com.wnd.entity.Vip;

import java.util.List;

public interface VipMapper {
    int deleteByPrimaryKey(Integer vid);

    int insert(Vip record);

    Vip selectByPrimaryKey(Integer vid);

    List<Vip> selectAll();

    int updateByPrimaryKey(Vip record);
}