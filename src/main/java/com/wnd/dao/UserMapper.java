package com.wnd.dao;

import com.wnd.entity.User;
import com.wnd.entity.vo.UserPermissionVo;

public interface UserMapper {

    UserPermissionVo findByUsername(String username);

    User selectByName(String username);

    int addUser(User user);
    User findByAddress(String  username);
}