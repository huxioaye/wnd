package com.wnd.dao;

import com.wnd.entity.Product;
import com.wnd.entity.vo.ProductPermissionVo;

import java.util.List;

public interface ProductMapper {

    ProductPermissionVo SelectAll(int id);
}