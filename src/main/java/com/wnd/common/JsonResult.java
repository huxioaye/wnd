package com.wnd.common;

public class JsonResult<T> {
    //状态  0  成功   1  失败  -1未登录或登录过期
    private int state;
    //错误消息
    private String msg;
    //返回的数据
    private T data;

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public JsonResult() {
    }

    public JsonResult(int state, String msg, T data) {
        this.state = state;
        this.msg = msg;
        this.data = data;
    }
}
