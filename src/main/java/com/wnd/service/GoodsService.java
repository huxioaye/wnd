package com.wnd.service;

import com.wnd.entity.Product;
import com.wnd.entity.vo.ProductPermissionVo;

import java.util.List;

public interface GoodsService {
    ProductPermissionVo SelectAll(int id);
}
