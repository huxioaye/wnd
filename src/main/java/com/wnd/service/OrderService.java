package com.wnd.service;

import com.wnd.entity.Car;
import com.wnd.entity.vo.CarPermissionVo;

import java.util.List;

public interface OrderService {

    int updateOrder(int userid, int productid, int shopid, int count);

    List<CarPermissionVo> selectOrderAll(int userid, int shopid);

    int deleteAllOrder(int userid, int shopid);
}
