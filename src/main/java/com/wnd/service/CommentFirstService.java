package com.wnd.service;

import com.wnd.entity.vo.CommentPermissionVo;

import java.util.List;

public interface CommentFirstService {
    List<CommentPermissionVo> selectCommentBySid(Integer  sid);
}
