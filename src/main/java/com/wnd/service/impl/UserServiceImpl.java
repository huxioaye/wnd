package com.wnd.service.impl;

import com.wnd.dao.UserMapper;
import com.wnd.entity.User;
import com.wnd.entity.vo.UserPermissionVo;
import com.wnd.service.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserMapper  userMapper;

    /**
     * 用户登录
     * @param username
     * @return
     */
    @Override
    public UserPermissionVo findByUsername(String username) {
        UserPermissionVo user1=userMapper.findByUsername(username);
        User user2=userMapper.findByAddress(username);
        if (user2!=null){
            user1.setAddress(user2.getAddress());
        }
        return user1;
    }

    /**
     * 查询用户名是否存在
     * @param username
     * @return
     */
    @Override
    public String selectByName(String username) {
        User  user=userMapper.selectByName(username);
        if (user==null){
            return "";
        }else {
            return "用户名重复";
        }
    }

    /**
     * 用户注册
     * @param username
     * @param password
     * @param phone
     * @return
     */
    @Override
    public String addUser(String username, String password, String phone) {
        User user=new User();
        user.setUsername(username);
        user.setPassword(password);
        user.setPhone(phone);
        if (userMapper.addUser(user)>0){
            System.out.println("1111111111111");
            return "添加成功";
        }else {
            System.out.println("22222222222222222");
            return "添加失败，请重试";
        }
    }
}
