package com.wnd.service.impl;

import com.wnd.dao.CommentFirstMapper;
import com.wnd.entity.vo.CommentPermissionVo;
import com.wnd.service.CommentFirstService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CommentFirstServiceImpl implements CommentFirstService {
    @Autowired
    private CommentFirstMapper commentFirstMapper;
    @Override
    public List<CommentPermissionVo> selectCommentBySid(Integer sid) {
        return commentFirstMapper.selectCommentBySid(sid);
    }
}
