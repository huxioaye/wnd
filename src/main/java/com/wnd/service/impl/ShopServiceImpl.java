package com.wnd.service.impl;

import com.wnd.dao.ShopMapper;
import com.wnd.dao.ShopSortMapper;
import com.wnd.entity.Shop;
import com.wnd.entity.ShopSort;
import com.wnd.entity.vo.ShopPermissionVo;
import com.wnd.service.ShopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ShopServiceImpl implements ShopService {
    @Autowired
    private ShopMapper shopMapper;
    @Autowired
    private ShopSortMapper shopSortMapper;

    @Override
    public List<ShopSort> selectAll() {
        return shopSortMapper.selectAll();
    }

    @Override
    public List<ShopPermissionVo> selectAllShop(String address,String cate) {
        String[]  s=address.split(".");
        Map map=new HashMap();
        for (int i=0;i<s.length;i++){
            if (i==0){
                map.put("province",s[0]);
            }
            if (i==1){
                map.put("city",s[1]);
            }
            if (i==2){
                map.put("area",s[2]);
            }
        }
        if (s.length==2){
            map.put("area",null);
        }
        if (cate==""||cate==null){
            map.put("cate",null);
        }else {
            map.put("cate",cate);
        }
        NumberFormat nf = NumberFormat.getNumberInstance();
        nf.setMaximumFractionDigits(2);
        nf.setRoundingMode(RoundingMode.UP);
        List<ShopPermissionVo>  list=shopMapper.selectAllShop(map);
        for (ShopPermissionVo shop:list){
             shop.setTime(String.valueOf((int)(Math.random()*60+15)));
             shop.setTimePrice(Double.valueOf(nf.format(Double.valueOf(shop.getTime())*0.05)));
             shop.setAvergePrice(Double.valueOf(nf.format(Double.valueOf(shop.getAvergePrice()))));
        }
        return list;
    }

    @Override
    public List<ShopPermissionVo> SelectShopByName(String shopName) {
        NumberFormat nf = NumberFormat.getNumberInstance();
        nf.setMaximumFractionDigits(2);
        nf.setRoundingMode(RoundingMode.UP);
        List<ShopPermissionVo>  list=shopMapper.SelectShopByName(shopName);
        for (ShopPermissionVo shop:list){
            shop.setTime(String.valueOf((int)(Math.random()*60+15)));
            shop.setTimePrice(Double.valueOf(nf.format(Double.valueOf(shop.getTime())*0.05)));
            shop.setAvergePrice(Double.valueOf(nf.format(Double.valueOf(shop.getAvergePrice()))));
        }
        return list;
    }
}
