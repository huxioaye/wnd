package com.wnd.service.impl;

import com.wnd.dao.OrderMapper;
import com.wnd.entity.Car;
import com.wnd.entity.Product;
import com.wnd.entity.vo.CarPermissionVo;
import com.wnd.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class OrderServiceImpl implements OrderService {
    @Autowired
    private OrderMapper orderMapper;
    @Override
    public int updateOrder(int userid, int productid, int shopid, int count) {
        Map map=new HashMap();
        map.put("userid",userid);
        map.put("productid",productid);
        map.put("shopid",shopid);
        Car car=orderMapper.selectOrder(map);
        Date date=new Date();
        DateFormat dateFormat=new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        int x=0;
        if (car==null){
            map.put("time",dateFormat.format(date));
            map.put("count",count);
            x=orderMapper.addOrder(map);
        }else {
            if (count<1){
                x=orderMapper.deleteOrder(map);
            }else{
                map.put("time",dateFormat.format(date));
                map.put("count",count);
                x=orderMapper.updateOrder(map);
            }
        }
        return x;
    }

    @Override
    public List<CarPermissionVo> selectOrderAll(int userid, int shopid) {
        Map map=new HashMap();
        map.put("userid",userid);
        map.put("shopid",shopid);
        return orderMapper.selectOrderAll(map);
    }

    @Override
    public int deleteAllOrder(int userid, int shopid) {
        Map map=new HashMap();
        map.put("userid",userid);
        map.put("shopid",shopid);
        return orderMapper.deleteAllOrder(map);
    }
}
