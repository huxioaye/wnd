package com.wnd.service.impl;

import com.wnd.dao.ProductMapper;
import com.wnd.entity.Product;
import com.wnd.entity.ProductNav;
import com.wnd.entity.Shop;
import com.wnd.entity.vo.ProductPermissionVo;
import com.wnd.entity.vo.ShopPermissionVo;
import com.wnd.service.GoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.RoundingMode;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class GoodsServiceImpl implements GoodsService {
    @Autowired
    private ProductMapper productMapper;
    @Override
    public ProductPermissionVo SelectAll(int id) {
        ProductPermissionVo productPermissionVo=productMapper.SelectAll(id);
        Map<String,List<Product>> map=new HashMap<>();
        for (int i=0;i<productPermissionVo.getProductNavs().size();i++){
            List<Product> list=new ArrayList<>();
            for (int j=0;j<productPermissionVo.getProducts().size();j++){
                if (productPermissionVo.getProducts().get(j).getSort().equals(productPermissionVo.getProductNavs().get(i).getSortName())){
                    list.add(productPermissionVo.getProducts().get(j));
                }
            }
            map.put("product"+i,list);
        }
        NumberFormat nf = NumberFormat.getNumberInstance();
        nf.setMaximumFractionDigits(2);
        nf.setRoundingMode(RoundingMode.UP);
        ShopPermissionVo shop=productPermissionVo.getShop();
        shop.setTime(String.valueOf((int)(Math.random()*60+15)));
        shop.setTimePrice(Double.valueOf(nf.format(Double.valueOf(shop.getTime())*0.05)));
        shop.setAvergePrice(Double.valueOf(nf.format(Double.valueOf(shop.getAvergePrice()))));
        productPermissionVo.setShop(shop);
        productPermissionVo.setProductsAndNav(map);
        return productPermissionVo;
    }
}
