package com.wnd.service;

import com.wnd.entity.Shop;
import com.wnd.entity.ShopSort;
import com.wnd.entity.vo.ShopPermissionVo;

import java.util.List;

public interface ShopService {
    List<ShopSort> selectAll();

    List<ShopPermissionVo> selectAllShop(String address,String cate);

    List<ShopPermissionVo> SelectShopByName(String shopName);
}
