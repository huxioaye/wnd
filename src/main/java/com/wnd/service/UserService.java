package com.wnd.service;

import com.wnd.entity.User;
import com.wnd.entity.vo.UserPermissionVo;

public interface UserService {
    UserPermissionVo  findByUsername(String  username);

    String selectByName(String username);

    String addUser(String username, String password, String phone);

}
