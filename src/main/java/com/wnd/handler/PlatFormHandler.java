package com.wnd.handler;

import com.wnd.enums.PlatFormEnums;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.TypeHandler;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * 订单状态
 */
public class PlatFormHandler implements TypeHandler<PlatFormEnums> {


    @Override
    public void setParameter(PreparedStatement preparedStatement, int i, PlatFormEnums orderEnums, JdbcType jdbcType) throws SQLException {
        preparedStatement.setObject(i,orderEnums.getCode());
    }

    @Override
    public PlatFormEnums getResult(ResultSet resultSet, String s) throws SQLException {
        return PlatFormEnums.getByCode(resultSet.getInt(s));
    }

    @Override
    public PlatFormEnums getResult(ResultSet resultSet, int i) throws SQLException {
        return PlatFormEnums.getByCode(resultSet.getInt(i));
    }

    @Override
    public PlatFormEnums getResult(CallableStatement callableStatement, int i) throws SQLException {
        return PlatFormEnums.getByCode(callableStatement.getInt(i));
    }
}
