package com.wnd.handler;

import com.wnd.enums.AuditEnums;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.TypeHandler;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class AuditHandler implements TypeHandler<AuditEnums> {


    @Override
    public void setParameter(PreparedStatement preparedStatement, int i, AuditEnums riderEnums, JdbcType jdbcType) throws SQLException {
        preparedStatement.setObject(i,riderEnums.getCode());
    }

    @Override
    public AuditEnums getResult(ResultSet resultSet, String s) throws SQLException {
        return AuditEnums.getByCode(resultSet.getInt(s));
    }

    @Override
    public AuditEnums getResult(ResultSet resultSet, int i) throws SQLException {
        return AuditEnums.getByCode(resultSet.getInt(i));
    }

    @Override
    public AuditEnums getResult(CallableStatement callableStatement, int i) throws SQLException {
        return AuditEnums.getByCode(callableStatement.getInt(i));
    }
}
