package com.wnd.handler;

import com.wnd.enums.PermissionEnums;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.TypeHandler;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class PermissionHandler implements TypeHandler<PermissionEnums> {
    @Override
    public void setParameter(PreparedStatement preparedStatement, int i, PermissionEnums permissionEnums, JdbcType jdbcType) throws SQLException {
        preparedStatement.setObject(i,permissionEnums.getCode());
    }

    @Override
    public PermissionEnums getResult(ResultSet resultSet, String s) throws SQLException {
        return PermissionEnums.getByCode(resultSet.getInt(s));
    }

    @Override
    public PermissionEnums getResult(ResultSet resultSet, int i) throws SQLException {
        return PermissionEnums.getByCode(resultSet.getInt(i));
    }

    @Override
    public PermissionEnums getResult(CallableStatement callableStatement, int i) throws SQLException {
        return PermissionEnums.getByCode(callableStatement.getInt(i));
    }
}
