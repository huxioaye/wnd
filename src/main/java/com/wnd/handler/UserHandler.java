package com.wnd.handler;

import com.wnd.enums.UserEnums;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.TypeHandler;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserHandler implements TypeHandler<UserEnums> {

    @Override
    public void setParameter(PreparedStatement preparedStatement, int i, UserEnums userEnums, JdbcType jdbcType) throws SQLException {
        preparedStatement.setObject(i,userEnums.getCode());
    }

    @Override
    public UserEnums getResult(ResultSet resultSet, String s) throws SQLException {
        return UserEnums.getByCode(resultSet.getInt(s));
    }

    @Override
    public UserEnums getResult(ResultSet resultSet, int i) throws SQLException {
        return UserEnums.getByCode(resultSet.getInt(i));
    }

    @Override
    public UserEnums getResult(CallableStatement callableStatement, int i) throws SQLException {
        return UserEnums.getByCode(callableStatement.getInt(i));
    }
}
