package com.wnd.handler;

import com.wnd.enums.UserStateEnums;
import org.apache.ibatis.type.JdbcType;
import org.apache.ibatis.type.TypeHandler;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class UserStateHandler implements TypeHandler<UserStateEnums> {


    @Override
    public void setParameter(PreparedStatement preparedStatement, int i, UserStateEnums userStateEnums, JdbcType jdbcType) throws SQLException {
        preparedStatement.setObject(i,userStateEnums.getCode());
    }

    @Override
    public UserStateEnums getResult(ResultSet resultSet, String s) throws SQLException {
        return UserStateEnums.getByCode(resultSet.getInt(s));
    }

    @Override
    public UserStateEnums getResult(ResultSet resultSet, int i) throws SQLException {
        return UserStateEnums.getByCode(resultSet.getInt(i));
    }

    @Override
    public UserStateEnums getResult(CallableStatement callableStatement, int i) throws SQLException {
        return UserStateEnums.getByCode(callableStatement.getInt(i));
    }
}
